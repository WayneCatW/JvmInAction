import edu.whut.util.ClassExecutor;

import java.io.*;
import java.net.URL;

/**
 * @program: JvmInAction
 * @description:
 * @author: Wayne
 * @create: 2020-05-28 11:09
 **/
public class Test {

    public static void main(String[] args) throws IOException, ClassNotFoundException {
        File file = new File("F:\\wwq\\src\\SayHello.class");
        URL url = Thread.currentThread().getContextClassLoader().getResource("edu/whut/io");
        long fileSize = file.length();
        FileInputStream fileInputStream = new FileInputStream(file);
        byte[] classByte = new byte[(int)fileSize];
        int offset = 0;
        int numRead = 0;
        while (offset < classByte.length && (numRead = fileInputStream.read(classByte, offset ,classByte.length - offset)) >= 0){
            offset += numRead;
        }
        fileInputStream.close();
        String result = ClassExecutor.execute(classByte);
        System.out.println(result);
    }
}
