package edu.whut.io;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintStream;
import java.lang.System;

/**
 * @program: JvmInAction
 * @description: 自定义信息收集器
 * @author: Wayne
 * @create: 2020-05-28 10:32
 **/
public class HackerSystem {
    public final static InputStream in = System.in;

    private static ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();

    public final static PrintStream out = new PrintStream(byteArrayOutputStream);

    public final static PrintStream err = out;

    public static String getString() {
        return byteArrayOutputStream.toString();
    }

    public static void clearBuffer() {
        byteArrayOutputStream.reset();
    }
}
