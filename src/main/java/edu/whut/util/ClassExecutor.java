package edu.whut.util;

/**
 * @program: JvmInAction
 * @description: JavaClass执行工具
 * @author: Wayne
 * @create: 2020-05-28 10:52
 **/

import edu.whut.classloader.HotSwapClassLoader;
import edu.whut.io.HackerSystem;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * 执行外部传来的一个代表Java类的byte数组
 * 执行过程：
 * 1. 清空HackSystem中的缓存
 * 2. new ClassModifier，并传入需要被修改的字节数组
 * 3. 调用ClassModifier#modifyUTF8Constant修改：
 *      java/lang/System -> edu/whut/io/HackerSystem
 * 4. new一个类加载器，把字节数组加载为Class对象
 * 5. 通过反射调用Class对象的main方法
 * 6. 从HackSystem中获取返回结果
 */
public class ClassExecutor {

    public static String execute(byte[] classByte){
        HackerSystem.clearBuffer();
        ClassModifier cm = new ClassModifier(classByte);
        byte[] modifyBytes = cm.modifyUTF8Constant("java/lang/System", "edu/whut/io/HackerSystem");
        HotSwapClassLoader classLoader = new HotSwapClassLoader();
        Class clazz = classLoader.loadByte(modifyBytes);
        try {
            Method method = clazz.getMethod("main", new Class[]{ String[].class });
            method.invoke(null, new String[]{null});

        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
        return HackerSystem.getString();
    }
}
